# Vagrant Template for Ubuntu

This file contains instructions on how to setup and manage a collection of [Vagrant instances](http://vagrantup.com).

## Creating Your First Server

First, create the config file. You can copy the template file.

```
 $ cp config.json.template config.json
```

Configure your instances. You can define a collection virtual machines.

Following you can see a example of the configuration of a instance.

### General Configuration

```
"test-01": {
    "enabled": true,
    "guest-hostname": "test-01.vm.server",
    "group": "Test Group",
    "box": "ubuntu/trusty64",
    "box-url": "http://files.vagrantup.com/precise64.box",
    "timezone": "Europe/Madrid",
    "cpus": 1,
    "memory": 2048,
    "ssh-prv-key": "~/.ssh/id_rsa",
    "ssh-pub-key": "~/.ssh/id_rsa.pub",
    "network": [
      {
        "network-type": "private",
        "if-adapter": "eth1",
        "if-inet-type": "static",
        "if-address": "10.0.3.50"
      }
    ],
    "scripts": [
        "bootstrap.sh",
        "ssh.sh"
    ]
},
```

| Configuration         |  Value(s)                                                         |
| --------------------- | ----------------------------------------------------------------- |
| enabled               | True if we want to provision the machine, false otherwise         |
| guest-hostname        | Hostname of the server                                            |
| group                 | If you want to group the instance. In VirtualBox, this option groups the instances under the same VirtualBox group in the interface |
| box                   | Vagrant Box                                                       |
| box-url               | URL to download the box                                           |
| timezone              | Server timezone                                                   |
| cpus                  | Number of CPUs                                                    |
| memory                | Instance memory                                                   |
| ssh-prv-key           | The private key to acces the instance                             |
| ssh-pub-key           | The public key to acces the instance                              |
| scripts               | List of script that we want to execute in the provisioning stage  |

### Network Configuration

```
"network": {
  "gw-ip": "192.168.0.1",
  "gw-if": "eth4",           
  "interfaces": [
    {
      "network-type": "private",
      "if-adapter": "eth1",
      "if-inet-type": "static",
      "if-address": "10.0.3.80",
      "if-netmask": "255.255.0.0"
    },
    {
      "network-type": "private",
      "if-adapter": "eth2",
      "if-address": "10.0.3.81"
    },
    {
      "network-type": "private",
      "if-adapter": "eth3"
    },
    {
      "network-type": "public",
      "if-adapter": "eth4",
      "if-inet-type": "static",
      "if-address": "192.168.0.157",
      "if-netmask": "255.255.0.0",
      "bridge-adapter": "enp1s0"
    },
    {
      "network-type": "public",
      "if-adapter": "eth5",
      "if-inet-type": "static",
      "if-address": "192.168.0.158"
    },
    {
      "network-type": "private",
      "if-adapter": "eth6",
      "if-inet-type": "dhcp"
    }
  ]
}
```

#### General Network Configuration

| Configuration         |  Value(s)                                                         |
| --------------------- | ----------------------------------------------------------------- |
| gw-ip                 | The gateway IP                                                    |
| gw-if                 | The network interface that we want to use in the network route    |

#### Interface Configuration

| Configuration         |  Value(s)                                                         |
| --------------------- | ----------------------------------------------------------------- |
| network-type          | Private or Public [Vagrant documentation](https://www.vagrantup.com/docs/getting-started/networking.html) |
| if-adapter            | The name of the network adapter. i.e. eth0, eth1, etc.    |
| if-inet-type          | Static or nothing. [Vagrant documentation](https://www.vagrantup.com/docs/getting-started/networking.html) |
| if-address            | Ip address for the interface    |
| if-netmask            | Netmask for the interface    |
| bridge-adapter        | Indicates that the interface is a ```bridge``` interface. [Vagrant documentation](https://www.vagrantup.com/docs/networking/public_network.html)    |

Finally, run the following command:

```
 $ vagrant up
```

This will create the machines based off the configuration settings in `config.json`.

## Manage the Instance

### View the Instances Status

```
 $ vagrant status
```

### Connecting via SSH

```
 $ vagrant ssh [instance-id]
```

### Suspending the Server

```
 $ vagrant suspend [instance-id]
```

### Halting the Server

```
 $ vagrant halt [instance-id]
```

### Destroying the Server

```
 $ vagrant destroy [instance-id]
```

## License

This project is licensed under the terms of the GNU General Public License, versión 3
